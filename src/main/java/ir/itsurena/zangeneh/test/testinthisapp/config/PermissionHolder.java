package ir.itsurena.zangeneh.test.testinthisapp.config;

import ir.itsurena.zangeneh.test.testinthisapp.model.Permission;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

@Component
public class PermissionHolder {

    public static ConcurrentHashMap<String, Permission> allPermissions = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, Permission> getAllPermissions() {
        return allPermissions;
    }

    public static void setAllPermissions(ConcurrentHashMap<String, Permission> allPermissions) {
        PermissionHolder.allPermissions = allPermissions;
    }

    public static Permission getByToken(String token) {
        return allPermissions.get(token);
    }


}
