package ir.itsurena.zangeneh.test.testinthisapp.Service;

import ir.itsurena.zangeneh.test.testinthisapp.config.PermissionHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UMSService umsService;

    @Autowired
    PasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String userName) {
        return loadFromCache(userName);
    }


    private UserDetails loadFromCache(String userName) {

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

        PermissionHolder.allPermissions.forEach((s, permission) -> {
            if (permission.getUserName().equals(userName)) {
                for (String servicePerm : permission.getServicePermissions()) {
                    grantedAuthorities.add(new SimpleGrantedAuthority(servicePerm));
                }
            }
        });

        if (grantedAuthorities.isEmpty()){
            throw new UsernameNotFoundException("UserName is not in our cache :" +userName);
        }
        return new User(userName,passwordEncoder.encode("ThisIsNotAValidPassWORD1"),true,true,true,true,grantedAuthorities);
    }


}