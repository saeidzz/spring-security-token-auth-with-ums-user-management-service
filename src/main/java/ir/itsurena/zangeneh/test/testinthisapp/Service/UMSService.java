package ir.itsurena.zangeneh.test.testinthisapp.Service;

import ir.itsurena.zangeneh.test.testinthisapp.JWTUtil;
import ir.itsurena.zangeneh.test.testinthisapp.model.Permission;
import ir.itsurena.zangeneh.test.testinthisapp.model.Token;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;

@Service
public class UMSService {

    public Permission getPermission(String jwt) {
        //calling ums service by jwt token

        HashSet<String> servicePermissions = new HashSet<>();
        servicePermissions.add("ROLE_USER");
        servicePermissions.add("ROLE_ADMIN");
        servicePermissions.add("ROLE_SUPER_ADMIN");

        Token token = null;
        try {
            token = (Token) JWTUtil.convertTokenStringToObject(jwt, Token.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Permission permission = new Permission(jwt, token.getPreferredUsername(), servicePermissions, null, new Date().getTime());

        return permission;
    }
}
