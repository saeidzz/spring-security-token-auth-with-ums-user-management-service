package ir.itsurena.zangeneh.test.testinthisapp.controller;

import ir.itsurena.zangeneh.test.testinthisapp.Service.UMSService;
import ir.itsurena.zangeneh.test.testinthisapp.config.PermissionHolder;
import ir.itsurena.zangeneh.test.testinthisapp.model.Permission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin("http://10.40.0.227:4200")
public class HomeController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UMSService umsService;


    Logger logger = LoggerFactory.getLogger(HomeController.class);


    @GetMapping("/getHeader")
    public ResponseEntity<?> getHeader() {
        return ResponseEntity.ok("123");
    }

    @PostMapping(value = "/getPermissions")
    public ResponseEntity<Permission> getPermissions(@RequestBody String jwt) {


        if (PermissionHolder.allPermissions.get(jwt) == null) {
            PermissionHolder.allPermissions.put(jwt, umsService.getPermission(jwt));
        }

        Permission permission = PermissionHolder.allPermissions.get(jwt);

      /*  List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for (String s : permission.getServicePermissions()) {
            grantedAuthorities.add(new SimpleGrantedAuthority(s));
        }*/

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        permission.getUserName(),
                        "ThisIsNotAValidPassWORD1"));


        SecurityContextHolder.getContext().setAuthentication(authentication);


        return new ResponseEntity(permission, HttpStatus.OK);
    }


}