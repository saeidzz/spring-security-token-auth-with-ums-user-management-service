package ir.itsurena.zangeneh.test.testinthisapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestinthisappApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestinthisappApplication.class, args);
    }

}
