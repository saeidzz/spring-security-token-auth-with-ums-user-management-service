package ir.itsurena.zangeneh.test.testinthisapp.model;

import java.util.HashSet;
import java.util.Objects;

public class Permission {
    private String token;
    private String userName;
    private HashSet<String> servicePermissions;
    private HashSet<String> menuPermissions;
    private Long firstRequestTime;

    public Permission() {
    }

    public Permission(String token, String userName, HashSet<String> servicePermissions, HashSet<String> menuPermissions, Long firstRequestTime) {
        this.token = token;
        this.userName = userName;
        this.servicePermissions = servicePermissions;
        this.menuPermissions = menuPermissions;
        this.firstRequestTime = firstRequestTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public HashSet<String> getServicePermissions() {
        return servicePermissions;
    }

    public void setServicePermissions(HashSet<String> servicePermissions) {
        this.servicePermissions = servicePermissions;
    }

    public HashSet<String> getMenuPermissions() {
        return menuPermissions;
    }

    public void setMenuPermissions(HashSet<String> menuPermissions) {
        this.menuPermissions = menuPermissions;
    }

    public Long getFirstRequestTime() {
        return firstRequestTime;
    }

    public void setFirstRequestTime(Long firstRequestTime) {
        this.firstRequestTime = firstRequestTime;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "token='" + token + '\'' +
                ", userName='" + userName + '\'' +
                ", servicePermissions=" + servicePermissions +
                ", menuPermissions=" + menuPermissions +
                ", firstRequestTime=" + firstRequestTime +
                '}';
    }


    public String toJson() {
        return "{\"Permission\":{"
                + "\"token\":\"" + token + "\""
                + ",\"userName\":\"" + userName + "\""
                + ",\"servicePermissions\":" + servicePermissions
                + ",\"menuPermissions\":" + menuPermissions
                + ",\"firstRequestTime\":\"" + firstRequestTime + "\""
                + "}}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Permission that = (Permission) o;
        return Objects.equals(token, that.token) &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(servicePermissions, that.servicePermissions) &&
                Objects.equals(menuPermissions, that.menuPermissions) &&
                Objects.equals(firstRequestTime, that.firstRequestTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(token, userName, servicePermissions, menuPermissions, firstRequestTime);
    }
}
